import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@IonicPage()
@Component({
  selector: 'page-send-data',
  templateUrl: 'send-data.html',
})
export class SendDataPage {
  public form: FormGroup;
  public lnombreDoc: any;
  public lapellidoDoc: any;
  public lcedulaDoc: any;
  public lemailDoc: any;
  public lnombreCubiculo:any;
  public lhorario:any;
  public lnombreDia:any;
  public assist : any;
  public isEdited: boolean = false;
  public hideForm: boolean = false;
  public pageTitle: string;
  public recordID: any = null;
  public selectDate: any;
  public ngObserver: any;
  public Available: any;
  public notAvailable:any;
  private baseURI: string  = "http://localhost:82/";
  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public NP: NavParams,
    public fb: FormBuilder,
    public toastCtrl: ToastController
  ) {
    this.form = fb.group({
      "nombreDoc": ["", Validators.required],
      "apellidoDoc": ["", Validators.required],
      "cedulaDoc": ["", Validators.required],
      "emailDoc": ["", Validators.required],
      "nombreCubiculo": ["", Validators.required],
      "horario": ["", Validators.required],
      "nombreDia": ["", Validators.required],
      "observacion" : ["", Validators.nullValidator],
    //  "av": ["", Validators.nullValidator],
   //  "nav": ["",Validators.nullValidator],
      "fecha": ["",Validators.required]


   
   });
  }
  ionViewWillEnter() : void
  {
     this.resetFields();

     if(this.NP.get("record"))
     {
        this.isEdited = true;
        this.selectEntry(this.NP.get("record"));
        this.pageTitle  = 'Registro Nuevo';
     }
     else
     {
        this.isEdited = false;
        this.pageTitle = 'Crear Entrada';
     }
  }
  selectEntry(item : any) : void
   {
      this.lnombreDoc = item.nombreDoc;
      this.lapellidoDoc = item.apellidoDoc;
      this.lcedulaDoc = item.cedulaDoc;
      this.lemailDoc = item.emailDoc;
      this.lnombreCubiculo = item.nombreCubiculo;
      this.lhorario = item.horario;
      this.lnombreDia = item.nombreDia;
      this.recordID = item.idDoc;
      
   }
   

   createEntry(nombDocente : string, apeDocente : string, cedDocente: string, emailDocente: string, cubDocente: string, horDocente: string, diaDocente: string, fechaAsistencia: string, estadoAsistencia: string, obsAsistencia: string ) : void
   {

      console.log( this.form.controls["nombreDoc"].value,
       this.form.controls["apellidoDoc"].value,
           this.form.controls["cedulaDoc"].value,
           this.form.controls["emailDoc"].value,
          this.form.controls["nombreCubiculo"].value,
           this.form.controls["horario"].value,
            this.form.controls["nombreDia"].value,
          this.form.controls["fecha"].value,
          this.getItemDay(),
        this.form.controls["observacion"].value)


      let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options 	: any		= { "key" : "crea", "nombDocente" : nombDocente , "apeDocente" : apeDocente, "cedDocente": cedDocente, "emailDocente": emailDocente, "cubDocente": cubDocente,"horDocente":horDocente,"diaDocente":diaDocente, "fechaAsistencia": fechaAsistencia, "estadoAsistencia" :estadoAsistencia, "obsAsistencia":obsAsistencia},
          url       : any      	= this.baseURI + "CRUD.php";

      this.http.post(url, JSON.stringify(options), headers)
      .subscribe((data : any) =>
      {
        
       //  console.log(error);
         this.sendNotification('Algo salió mal al crear!');
      },
      (error : any) =>
      {

        // console.log(data);
         this.hideForm   = true;
         this.sendNotification(`El usuario : ${nombDocente} fue agregado correctamente`);
       
      });
   }
   updateEntry(nombreDoc : string, apellidoDoc : string, cedulaDoc: string, emailDoc: string) : void
   {
      let headers : any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options : any		= { "key" : "update", "nombreDoc" : nombreDoc, "apellidoDoc" : apellidoDoc, "cedulaDoc": cedulaDoc, "emailDoc": emailDoc, "recordID" : this.recordID},
          url  : any = "http://localhost:82/CRUD.php";

      this.http
      .post(url, JSON.stringify(options), headers)
      .subscribe(data =>
      {
         // If the request was successful notify the user
         this.hideForm  =  true;
         this.sendNotification(`El usuario: ${nombreDoc} fue actualizado correctamente`);
      },
      (error : any) =>
      {
         this.sendNotification('Algo salió mal!');
      });
   }
   deleteEntry() : void
   {
      let nombre: string 	= this.form.controls["nombreDoc"].value,
          headers: any	= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options: any = { "key" : "delete", "recordID" : this.recordID},
          url: any = this.baseURI + "CRUD.php";

      this.http
      .post(url, JSON.stringify(options), headers)
      .subscribe(data =>
      {
         
      },
      (error : any) =>
      {
         this.hideForm     = true;
         this.sendNotification(`El usuario : ${nombre} fue eliminado correctamente`);
         //this.sendNotification('algo salió mal!');
      });
   }



   sendNotification(message : string)  : void
   {
      let notification = this.toastCtrl.create({
          message : message,
          duration : 3000
      });
      notification.present();
   }
   saveEntry() : void
   {
      this.isEdited=false;

      let nombDocente : string = this.form.controls["nombreDoc"].value,
          apeDocente: string    = this.form.controls["apellidoDoc"].value,
          cedDocente: string    = this.form.controls["cedulaDoc"].value,
          emailDocente: string    = this.form.controls["emailDoc"].value,
          cubDocente: string    = this.form.controls["nombreCubiculo"].value,
          horDocente: string    = this.form.controls["horario"].value,
          diaDocente: string    = this.form.controls["nombreDia"].value,
          fechaAsistencia: string    = this.form.controls["fecha"].value,
          estadoAsistencia: string    = this.assist,
          obsAsistencia: string    = this.form.controls["observacion"].value;
          if(this.isEdited)
          {
             console.log("entro");
          }
          else
          {
            this.createEntry(nombDocente,  apeDocente, cedDocente, emailDocente, cubDocente, horDocente,diaDocente, fechaAsistencia, estadoAsistencia, obsAsistencia
               );
          }
         
      
   }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SendDataPage');
  }
  getItemDay(){
     var aux;
     if((this.form.controls["observacion"].value)==""){
      aux= "No hay Observación"
     }else{
        aux= this.ngObserver;
     }
     return aux;
  }

  getObserver(){
   
   console.log(this.getItemDay())
    
  
    // console.log(this.form.controls["observacion"].value);
     console.log(this.assist);
     console.log(this.form.controls["fecha"].value);
  }
  check1(co){
 console.log(co);
 if(co==true){
    this.assist="Disponible";
 }else{
    this.assist="No Disoponible";
 }
  }

  resetFields() : void
   {
      this.lnombreDoc = "";
      this.lapellidoDoc = "";
      this.lcedulaDoc = "";
      this.lemailDoc = "";
      this.lnombreCubiculo= "";
      this.lhorario = "";
      this.lnombreDia = "";
     
   }
 
 }

