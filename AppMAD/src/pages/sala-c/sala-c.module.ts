import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalaCPage } from './sala-c';

@NgModule({
  declarations: [
    SalaCPage,
  ],
  imports: [
    IonicPageModule.forChild(SalaCPage),
  ],
})
export class SalaCPageModule {}
