import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-view-teacher',
  templateUrl: 'view-teacher.html',
})
export class ViewTeacherPage {
  public items : Array<any> = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewTeacherPage');
  }

  ionViewWillEnter() : void
  {
     this.load();
  }


  load() : void
   {
      this.http
      .get('http://localhost:82/SelectDoc.php')
      .subscribe((data : any) =>
      {
         console.dir(data);
         this.items = data;
      },
      (error : any) =>
      {
         console.dir(error);
      });
   }

   addEntry() : void
   {
      this.navCtrl.push('CrudTeacherPage');
   }

   viewEntry(param : any) : void
   {
      this.navCtrl.push('CrudTeacherPage', param);
   }
}
