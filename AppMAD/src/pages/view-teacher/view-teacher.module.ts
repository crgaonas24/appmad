import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewTeacherPage } from './view-teacher';

@NgModule({
  declarations: [
    ViewTeacherPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewTeacherPage),
  ],
})
export class ViewTeacherPageModule {}
