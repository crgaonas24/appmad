import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CrudTeacherPage } from './crud-teacher';

@NgModule({
  declarations: [
    CrudTeacherPage,
  ],
  imports: [
    IonicPageModule.forChild(CrudTeacherPage),
  ],
})
export class CrudTeacherPageModule {}
