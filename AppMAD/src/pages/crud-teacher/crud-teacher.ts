import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
/**
 * Generated class for the CrudTeacherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crud-teacher',
  templateUrl: 'crud-teacher.html',
})
export class CrudTeacherPage {
  public form: FormGroup;
  public lnombreDoc: any;
  public lapellidoDoc: any;
  public lcedulaDoc: any;
  public lemailDoc: any;
  public isEdited: boolean = false;
  public hideForm: boolean = false;
  public pageTitle: string;
  public recordID: any = null;
  private baseURI: string  = "http://localhost:82/";
  constructor(public navCtrl: NavController,
    public http: HttpClient,
    public NP: NavParams,
    public fb: FormBuilder,
    public toastCtrl: ToastController) {

      this.form = fb.group({
        "nombreDoc": ["", Validators.required],
        "apellidoDoc": ["", Validators.required],
        "cedulaDoc": ["", Validators.required],
        "emailDoc": ["", Validators.required]
     });
  }

  ionViewWillEnter() : void
   {
      this.resetFields();

      if(this.NP.get("record"))
      {
         this.isEdited = true;
         this.selectEntry(this.NP.get("record"));
         this.pageTitle  = 'Modificar entrada';
      }
      else
      {
         this.isEdited = false;
         this.pageTitle = 'Crear Entrada';
      }
   }
   selectEntry(item : any) : void
   {
      this.lnombreDoc = item.nombreDoc;
      this.lapellidoDoc = item.apellidoDoc;
      this.lcedulaDoc = item.cedulaDoc;
      this.lemailDoc = item.emailDoc;
      this.recordID = item.idDoc;
   }

   createEntry(nombreDoc : string, apellidoDoc : string, cedulaDoc: string, emailDoc: string) : void
   {
      let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options 	: any		= { "key" : "create", "nombreDoc" : nombreDoc, "apellidoDoc" : apellidoDoc, "cedulaDoc": cedulaDoc, "emailDoc": emailDoc },
          url       : any      	= this.baseURI + "CRUD.php";

      this.http.post(url, JSON.stringify(options), headers)
      .subscribe((data : any) =>
      {
         // If the request was successful notify the user
         console.log(data);
         this.hideForm   = true;
         this.sendNotification(`El usuario : ${nombreDoc} fue agregado correctamente`);
      },
      (error : any) =>
      {
        console.log(error);
         this.sendNotification('Algo salió mal al crear!');
      });
   }

   updateEntry(nombreDoc : string, apellidoDoc : string, cedulaDoc: string, emailDoc: string) : void
   {
      let headers : any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options : any		= { "key" : "update", "nombreDoc" : nombreDoc, "apellidoDoc" : apellidoDoc, "cedulaDoc": cedulaDoc, "emailDoc": emailDoc, "recordID" : this.recordID},
          url  : any = "http://localhost:82/CRUD.php";

      this.http
      .post(url, JSON.stringify(options), headers)
      .subscribe(data =>
      {
         // If the request was successful notify the user
         this.hideForm  =  true;
         this.sendNotification(`El usuario: ${nombreDoc} fue actualizado correctamente`);
      },
      (error : any) =>
      {
         this.sendNotification('Algo salió mal!');
      });
   }

   deleteEntry() : void
   {
      let nombre: string 	= this.form.controls["nombreDoc"].value,
          headers: any	= new HttpHeaders({ 'Content-Type': 'application/json' }),
          options: any = { "key" : "delete", "recordID" : this.recordID},
          url: any = this.baseURI + "CRUD.php";

      this.http
      .post(url, JSON.stringify(options), headers)
      .subscribe(data =>
      {
         
      },
      (error : any) =>
      {
         this.hideForm     = true;
         this.sendNotification(`El usuario : ${nombre} fue eliminado correctamente`);
         //this.sendNotification('algo salió mal!');
      });
   }
   saveEntry() : void
   {
      let nombreDoc : string = this.form.controls["nombreDoc"].value,
          apellidoDoc: string    = this.form.controls["apellidoDoc"].value,
          cedulaDoc: string    = this.form.controls["cedulaDoc"].value,
          emailDoc: string    = this.form.controls["emailDoc"].value;

      if(this.isEdited)
      {
         this.updateEntry(nombreDoc, apellidoDoc, cedulaDoc, emailDoc);
      }
      else
      {
         this.createEntry(nombreDoc, apellidoDoc, cedulaDoc, emailDoc);
      }
   }

   resetFields() : void
   {
      this.lnombreDoc = "";
      this.lapellidoDoc = "";
      this.lcedulaDoc = "";
      this.lemailDoc = "";
   }

   sendNotification(message : string)  : void
   {
      let notification = this.toastCtrl.create({
          message : message,
          duration : 3000
      });
      notification.present();
   }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CrudTeacherPage');
  }

}
