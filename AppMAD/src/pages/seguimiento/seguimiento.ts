import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { DataProvider } from '../../providers/data/data';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

@IonicPage()
@Component({
  selector: 'page-seguimiento',
  templateUrl: 'seguimiento.html',
})
export class SeguimientoPage {
  searchTerm: string = '';
 searchControl: FormControl;
 items: any;
 searching: any = false;
 
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient, public dataService: DataProvider) {
 
    this.searchControl = new FormControl();
    this.load()
  }

 /* ionViewDidLoad() {
 
    this.setFilteredItems();
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
    this.searching = false;
    this.setFilteredItems();
    });
  }
  onSearchInput(){
    this.searching = true;
   }
   setFilteredItems() {
    this.items = this.dataService.filterItems(this.searchTerm);
    console.log(this.items);
   }
 /* ionViewWillEnter() : void
  {
     this.load();
  }*/


  load() : void
   {
      this.http
      .get('http://localhost:82/RegistroAsistencia.php')
      .subscribe((data : any) =>
      {
         console.dir(data);
         this.items = data;
      },
      (error : any) =>
      {
         console.dir(error);
      });
   }

 /*addEntry() : void
   {
      this.navCtrl.push('CrudTeacherPage');
   }

   viewEntry(param : any) : void
   {
      this.navCtrl.push('CrudTeacherPage', param);
   }*/
}
