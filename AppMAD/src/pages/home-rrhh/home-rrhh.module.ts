import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeRrhhPage } from './home-rrhh';

@NgModule({
  declarations: [
    HomeRrhhPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeRrhhPage),
  ],
})
export class HomeRrhhPageModule {}
