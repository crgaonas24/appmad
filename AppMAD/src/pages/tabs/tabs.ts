import { Component } from '@angular/core';
//import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SalaAPage} from '../sala-a/sala-a'
import {SalaBPage} from '../sala-b/sala-b'
import {SalaCPage} from '../sala-c/sala-c'
import {SeguimientoPage} from '../seguimiento/seguimiento'
/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  /*constructor(public navCtrl: NavController, public navParams: NavParams) {
  }*/
tab1Root = SalaAPage;
tab2Root = SalaBPage;
tab3Root = SalaCPage;
tab4Root = SeguimientoPage;

constructor(){
  
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
