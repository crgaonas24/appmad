import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalaAPage } from './sala-a';

@NgModule({
  declarations: [
    SalaAPage,
  ],
  imports: [
    IonicPageModule.forChild(SalaAPage),
  ],
})
export class SalaAPageModule {}
