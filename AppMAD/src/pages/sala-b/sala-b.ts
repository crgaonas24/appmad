import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';



@IonicPage()
@Component({
  selector: 'page-sala-b',
  templateUrl: 'sala-b.html',
})
export class SalaBPage {

  list: any;
  listTable: any;
  listSCHEDULE: any;
  listDay: any;
  listALL: any;
  itemSelectedTable: any;
  itemSelectedSchedule: any;
  itemSelectedDay: any;
  getitemSelectedTable: any;
  getitemSelectedSchedule: any;
  getitemSelectedDay: any;

  nombreCubiculo: any;
  flag;
  //public items : Array<any> = [];
  //getSelected;
  //element:any;
  // utlBd: string = "http://localhost:82/";

  //nombreUser:any;
  contador;
  public items: Array<any> = [];
  public items2: Array<any> = [];
 // lt: List
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient) {
    this.flag = false;
    //this.load();
    //this.loadTable();
    this.loadSCHEDULE();
    this.loadDay();
    //this.items;
    //this.loadAll();
    //this.loadAll();
    /*this.view("", "", "",
      "", "", "", "");*/
    //  this.loadAll();
    this.loadAll();
  }
 

  getItemTable(cod_operativo) {
    //this.flag =true;
    this.getitemSelectedTable = cod_operativo;
    //console.log(cod_operativo);
  }
  getItemSchedule(cod_operativo) {
    // this.flag =true;
    this.getitemSelectedSchedule = cod_operativo;
  }
  getItemDay(cod_operativo) {

    this.getitemSelectedDay = cod_operativo;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalaBPage');
  }

  loadTable() {
    this.http
      .get('http://localhost:82/cubiculo.php')
      .subscribe((data: any) => {
        this.listTable = data;
        //console.log(data);
      },
        (error: any) => {
          console.dir(error);
        });
  }



  loadSCHEDULE() {
    this.http
      .get('http://localhost:82/horarios.php')
      .subscribe((data: any) => {
        this.listSCHEDULE = data;
      },
        (error: any) => {
          console.dir(error);
        });
  }

  loadDay() {
    this.http
      .get('http://localhost:82/dias.php')
      .subscribe((data: any) => {

        this.listDay = data;

      },
        (error: any) => {
          console.dir(error);
        });
  }



  search() {

    this.itemSelectedTable;
    this.itemSelectedSchedule;
    this.itemSelectedDay;

  }

  ionViewWillEnter(): void {
    //this.load();
    //his.loadAll();
    // this.items;
    //this.view();
    // this.view("","","","","","","");
  }

  loadAll() {
    // this.flag = true;
    this.http
      .get('http://localhost:82/SalaB.php')
      .subscribe((data: any) => {







        for (let x of data) {
          if ((x.horario == this.getitemSelectedSchedule) && (x.nombreDia == this.getitemSelectedDay))
            this.items.push({
              nombreDoc: x.nombreDoc, apellidoDoc: x.apellidoDoc, cedulaDoc: x.cedulaDoc,
              emailDoc: x.emailDoc, nombreCubiculo: x.nombreCubiculo, horario: x.horario, nombreDia: x.nombreDia
            })
          // this.items = x
          console.log(x.nombreDoc)



        }



      },
        (error: any) => {
          console.dir(error);
        });
  }


  view() {


    /* this.items.forEach(element => {
       this.items2.push(element);
     });
     console.log(this.items2);*/
    for (let x of this.items) {

      this.items2.push({ nombreDoc: x.nombreDoc })
    }
    console.log(this.items2)
  }



  viewEntry(param: any): void {

    this.navCtrl.push('SendDataPage', param);
    //this.items.pop();
   this. reiniciar();
   this.deleteItems();
  }

  reiniciar() {
    for (let x of this.items) {
      if ((x.horario == this.getitemSelectedSchedule) && (x.nombreDia == this.getitemSelectedDay))
        this.items.length = 0;
      // this.items = x
      console.log(x.nombreDoc)


    }
    
  }

  deleteItems(){
    this.itemSelectedDay = "Select One";
    this.itemSelectedSchedule = "Select One";
  }

}
