import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalaBPage } from './sala-b';

@NgModule({
  declarations: [
    SalaBPage,
  ],
  imports: [
    IonicPageModule.forChild(SalaBPage),
  ],
})
export class SalaBPageModule {}
