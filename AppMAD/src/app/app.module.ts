import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';

import { SalaAPage } from '../pages/sala-a/sala-a';
import { SalaBPage } from '../pages/sala-b/sala-b';
import { SalaCPage } from '../pages/sala-c/sala-c';
import {SeguimientoPage} from '../pages/seguimiento/seguimiento'

import { TabsPage } from '../pages/tabs/tabs';
import { DataProvider } from '../providers/data/data';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SalaAPage,
    SalaBPage,
    SalaCPage,
    SeguimientoPage, 
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SalaAPage,
    SalaBPage,
    SalaCPage,
    SeguimientoPage, 
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider
  ]
})
export class AppModule {}
