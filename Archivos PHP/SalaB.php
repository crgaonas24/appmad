<?php
header("Access-Control-Allow-Origin:http://localhost:8100");
header("Content-Type: application/x-www-form-urlencoded");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = '';
   $db      = 'rrhh-ultimo';
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);
   $data    = array();


   // Attempt to query database table and retrieve data
   try {
      $stmt 	= $pdo->query('SELECT s.Sala, c.nombreCubiculo, d.nombreDia, h.horario, doc.nombreDoc, doc.apellidoDoc, doc.cedulaDoc, emailDoc
FROM sala s, cubiculo c, dia d, horario h, cubiculohorario ch, diacubiculo dc, docente doc
WHERE c.idSala = s.idSala and (c.idCubiculo = ch.idCubiculo)
and (h.idHorario = ch.idHorario) 
and (c.idCubiculo = dc.idCubiculo) 
and (d.idDia = dc.idDia) 
and (doc.idDocente = c.idDocente) and (s.Sala = "B") ');
      while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $data[] = $row;
      }

      // Return data as JSON
      echo json_encode($data);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }


?>