<?php
header("Access-Control-Allow-Origin:http://localhost:8100");
header("Content-Type: application/x-www-form-urlencoded");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

//Definir los parametros de conexión a la base de datos
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = '';
   $db      = 'rrhh-ultimo';
   $cs      = 'utf8';

//Configuración de parametros PDO
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Crear una instancia de PDO (conectarse a la base de datos)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);


   // Recuperar datos de la aplicación y convertirlos a .json
   $json    =  file_get_contents('php://input');
   $obj     =  json_decode($json);
   $key     =  strip_tags($obj->key);


   // Determina el caso que se ejecuta
   switch($key)
   {

      // Agregar un nuevo registro a la tabla docente
      case "create":

         // Desinfectar los valores de URL proporcionados
         $nombreDoc = filter_var($obj->nombreDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $apellidoDoc = filter_var($obj->apellidoDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $cedulaDoc	  = filter_var($obj->cedulaDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $emailDoc	  = filter_var($obj->emailDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);

         // Intento de ejecutar la declaración preparada POD
         try {
            $sql 	= "INSERT INTO docente(nombreDoc, apellidoDoc, cedulaDoc, emailDoc) VALUES(:nombreDoc, :apellidoDoc, :cedulaDoc, :emailDoc)";
            $stmt 	= $pdo->prepare($sql);
            $stmt->bindParam(':nombreDoc', $nombreDoc, PDO::PARAM_STR);
            $stmt->bindParam(':apellidoDoc', $apellidoDoc, PDO::PARAM_STR);
            $stmt->bindParam(':cedulaDoc', $cedulaDoc, PDO::PARAM_STR);
            $stmt->bindParam(':emailDoc', $emailDoc, PDO::PARAM_STR);
            $stmt->execute();

            echo json_encode(array('message' => 'El usuario' . $nombreDoc . '  fue ingresado con exito'));
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }

      break;
           
           
      // Actualización de un registro existente
      case "update":

         // Desinfectar los valores de URL proporcionados
         $nombreDoc = filter_var($obj->nombreDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $apellidoDoc = filter_var($obj->apellidoDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $cedulaDoc	  = filter_var($obj->cedulaDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $emailDoc	  = filter_var($obj->emailDoc, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $recordID = filter_var($obj->recordID, FILTER_SANITIZE_NUMBER_INT);

         // Intento de ejecutar la declaración preparada POD
         try {
            $sql 	= "UPDATE docente SET nombreDoc = :nombreDoc, apellidoDoc = :apellidoDoc, cedulaDoc = :cedulaDoc, emailDoc = :emailDoc WHERE idDoc = :recordID";
            $stmt 	=	$pdo->prepare($sql);
            $stmt->bindParam(':nombreDoc', $nombreDoc, PDO::PARAM_STR);
            $stmt->bindParam(':apellidoDoc', $apellidoDoc, PDO::PARAM_STR);
            $stmt->bindParam(':cedulaDoc', $cedulaDoc, PDO::PARAM_STR);
            $stmt->bindParam(':emailDoc', $emailDoc, PDO::PARAM_STR);
            $stmt->bindParam(':recordID', $recordID, PDO::PARAM_INT);
            $stmt->execute();

            echo json_encode('El usuario' . $nombreDoc . ' fue actualizado');
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }

      break;
           
           // Eliminar un registro existente en la tabla de tecnologías.
      case "delete":

         // Desinfectar el ID de registro proporcionado para hacer coincidir con el registro de la tabla
         $recordID	=	filter_var($obj->recordID, FILTER_SANITIZE_NUMBER_INT);

         // Intento de ejecutar la declaración preparada POD
         try {
            $pdo 	= new PDO($dsn, $un, $pwd);
            $sql 	= "DELETE FROM docente WHERE idDoc = :recordID";
            $stmt 	= $pdo->prepare($sql);
            $stmt->bindParam(':recordID', $recordID, PDO::PARAM_INT);
            $stmt->execute();

            echo json_encode('El usuario ' . $nombreDoc . ' fue eliminado');
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }

      break;
           
            // Agregar un nuevo registro a la tabla docente
      case "crea":

         // Desinfectar los valores de URL proporcionados
         $nombDocente = filter_var($obj->nombDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $apeDocente = filter_var($obj->apeDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $cedDocente	  = filter_var($obj->cedDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $emailDocente	  = filter_var($obj->emailDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $cubDocente	  = filter_var($obj->cubDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $horDocente	  = filter_var($obj->horDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $diaDocente	  = filter_var($obj->diaDocente, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $fechaAsistencia	  = filter_var($obj->fechaAsistencia, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $estadoAsistencia	  = filter_var($obj->estadoAsistencia, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);
         $obsAsistencia	  = filter_var($obj->obsAsistencia, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_LOW);


         // Intento de ejecutar la declaración preparada POD
         try {
            $sql 	= "INSERT INTO prueba(nombDocente,  apeDocente, cedDocente, emailDocente, cubDocente, horDocente,diaDocente, fechaAsistencia, estadoAsistencia, obsAsistencia) VALUES(:nombDocente,  :apeDocente, :cedDocente, :emailDocente, :cubDocente, :horDocente, :diaDocente, :fechaAsistencia, :estadoAsistencia, :obsAsistencia)";
            $stmt 	= $pdo->prepare($sql);
            $stmt->bindParam(':nombDocente', $nombDocente, PDO::PARAM_STR);
            $stmt->bindParam(':apeDocente', $apeDocente, PDO::PARAM_STR);
            $stmt->bindParam(':cedDocente', $cedDocente, PDO::PARAM_STR);
            $stmt->bindParam(':emailDocente', $emailDocente, PDO::PARAM_STR);
            $stmt->bindParam(':cubDocente', $cubDocente, PDO::PARAM_STR);
            $stmt->bindParam(':horDocente', $horDocente, PDO::PARAM_STR);
            $stmt->bindParam(':diaDocente', $diaDocente, PDO::PARAM_STR);
            $stmt->bindParam(':fechaAsistencia', $fechaAsistencia, PDO::PARAM_STR);
            $stmt->bindParam(':estadoAsistencia', $estadoAsistencia, PDO::PARAM_STR);
            $stmt->bindParam(':obsAsistencia', $obsAsistencia, PDO::PARAM_STR);
         
            $stmt->execute();

            echo json_encode(array('message' => 'El usuario' . $nombreDocente . '  fue Registrado con exito'));
         }
         // Catch any errors in running the prepared statement
         catch(PDOException $e)
         {
            echo $e->getMessage();
         }

      break;
           
   }
?>